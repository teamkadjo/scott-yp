# -*- coding: utf-8 -*-
import sys
import os
import django
import datetime
from scrapy.exceptions import DropItem
from django.db import IntegrityError

#print "Output is " + os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "..")

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "../ypfront"))
os.environ['DJANGO_SETTINGS_MODULE'] = 'ypfront.settings'
django.setup()




from ypfront import settings
from ypfront.models import *


class YpPipeline(object):
    def process_item(self, item, spider):
        if item["type"] == "yp":
            email_count = EntityInformation.objects.filter(email=item['email'],phone=item['phone']).count()
            if (email_count == 0):
                try:
                    ei = EntityInformation()
                    ei.business_name = item['business_name'].strip()
                    ei.phone = item['phone']
                    ei.status = item['status_text'].strip()
                    ei.website = item['website_link'].strip()
                    ei.email = item['email'].replace("mailto:","").strip()
                    ei.place = item['place']
                    ei.yp_link = item['yp_orig_link']
                    ei.address = item['address']
                    sinfo = StateInfo.objects.filter(statecode=item['state']).first()
                    ei.state = sinfo
                    ei.search_term = 'pest control'
                    ei.scrape_type = item["type"]
                    ei.save()   
                    print "Done saving {0}".format(item['business_name'])
                    return item
                except IntegrityError:
                    DropItem("Duplicate email %s and phone %s " % (item['email'],item['phone']))
            else:
                DropItem("Duplicate email %s and phone %s " % (item['email'],item['phone']))
        elif item["type"] == "state":
            state = StateInfo.objects.filter(statecode = item['statecode']).count()
            if state == 0:
                st = StateInfo()
                st.statecode = item['statecode']
                st.statename = item['statename']
                st.save()
            else:
                DropItem("Duplicate state %s " % (item['statename']))
        elif item["type"] == "city":            
            ct = CityInfo()
            ct.cityname = item['cityname']
            ct.source = item['source']
            the_source = item['source']
            citysplit = the_source.split("_in_")
            ct.state = citysplit[1].strip()
            ct.save()            
        elif item['type'] =='gp':
            print '************************',item['id']
            ei_object = EntityInformation.objects.filter(id=item['id']).first()
            if ((item['orig_url'] != item['new_url']) and (item['new_url'])):
                ei_object.is_with_portal = True
                ei_object.customer_portal = item['new_url']
                print "Done saving......*******************"
                ei_object.save()  
            else:
                ei_object.is_with_portal = False
                ei_object.save()
            pass

