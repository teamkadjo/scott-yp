# -*- coding: utf-8 -*-
import random 
import os
import sys
import datetime
import time
import random 
import scrapy
from scrapy.http import Request
import urlparse
import logging
import django

from scrapy.exceptions import DropItem

print "Output is " + os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "..")

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "../../ypfront"))
os.environ['DJANGO_SETTINGS_MODULE'] = 'ypfront.settings'
django.setup()

from ypfront.models import EntityInformation

agents = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
]


class GetPortals(scrapy.Spider):
    name = 'getportals'
    term = 'Columbus OH'    
    start_urls = ["https://en.wikipedia.org/wiki/List_of_cities_in_Ohio"]
    
    def start_requests(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            filename='yellowpage.log',
                            filemode='w+')        
        proxy_meta = "108.59.14.208:13081"

        #############################################################################
        ety_with_websites =  EntityInformation.objects.filter(website__isnull=False,search_term='pest control',state__statecode='NV').exclude(website__exact='').all()
        #############################################################################
        count = 0
        for c in ety_with_websites:           
            the_url = c.website
            print the_url
            the_meta = {}
            the_meta['orig_url'] = the_url          
            the_meta['id']   = c.id
            yield Request(the_url,self.parse,headers={'User-Agent': random.choice(agents)},meta=the_meta)
            #if count < 10:
                #count = count + 1
            #else:
                #time.sleep(random.choice(range(40)) + 20)
                #count = 0
        #yield Request(self.start_urls[0],self.parse_cities,headers={'User-Agent': random.choice(agents)},meta={"proxy":proxy_meta})
        
    def get_first(self,response):
        text_tag = ['LOGIN','ACCOUNT','PORTAL']
        for t in text_tag:
            rr = response.xpath("//a/*[contains(translate(.,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'" + t + "')]")
            
            if (len(rr)> 0):
                for j in rr:
                    if (j.root.text):
                        if (j.root.text.upper().find(t) > -1):
                            if (j.root.getparent().tag == 'a'):
                                if ('href' in j.root.getparent().attrib):
                                    return j.root.getparent().attrib['href']
                                else:
                                    #print j.root.getparent().attrib
                                    continue
                            elif (j.root.tag == 'a'):
                                if ('href' in j.root.attrib):
                                    return j.root.attrib['href']
                                else:
                                    continue
                            else:
                                return None
                    else:
                        continue
            else:
                #print "goes to else..{}".format(t) 
                rr = response.xpath("//a[contains(translate(.,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'" + t + "')]")
                #print rr
                for j in rr:
                    #print j.attrib
		    print j
                    if 'href' in j.root.attrib:
                        #print "there is href"
                        if self.is_absolute(j.root.attrib['href']):
                            return j.root.attrib['href']
                        else:
                            return urlparse.urljoin(response.url,j.root.attrib['href'])
                    else:
                        print "no href"                        
                    if (j.root.text):
                        if (j.root.text.upper().find(t) > -1):
                            if (j.root.getparent().tag == 'a'):
                                if ('href' in j.root.getparent().attrib):
                                    return j.root.getparent().attrib['href']
                                else:
                                    #print j.root.getparent().attrib
                                    continue
                            elif (j.root.tag == 'a'):
                                if ('href' in j.root.attrib):
                                    return j.root.attrib['href']
                                else:
                                    continue
                            else:
                                return None
                    else:
                        continue                
        return None
        
    def is_absolute(self,url):
        return bool(urlparse.urlparse(url).netloc)
    
    def parse(self, response):
        # checking whether there is portal or not on the given page of the entity
        logging.info("Parsing now this page : {}".format(response.request.url))
        if (response.status == 200):
            the_href = self.get_first(response)
            #the_result = response.xpath("//a[contains(translate(.,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLOMNOPQRSTUVWXYZ'),'LOGIN')]")[0].attrib['href']
            #if (len(the_result) >  1):
                ##get only one
                #the_href = the_result[0].attrib["href"]
                #yield({'new_url':response.url,'orig_url':response.request.url,'type':'gp','id':response.request.meta['id']})                
            #elif (len(the_result) == 1):
                #the_href = the_result.attrib["href"]
                #yield({'new_url':response.url,'orig_url':response.request.url,'type':'gp','id':response.request.meta['id']})
            #else:
            if the_href:
                yield({'new_url':the_href,'orig_url':response.request.url,'type':'gp','id':response.request.meta['id']})
            else:
                yield({'new_url':None,'orig_url':response.request.url,'type':'gp','id':response.request.meta['id']})
        else: # the fetching fails because there is error while getting info on the link
                yield({'new_url':None,'orig_url':response.request.url,'type':'gp','id':response.request.meta['id']})
            
