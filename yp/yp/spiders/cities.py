# -*- coding: utf-8 -*-
import random 
import scrapy
from scrapy.http import Request
import urlparse
import logging
import time
import random 

agents = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
]


class CitySpider(scrapy.Spider):
    name = 'uscities'  
    start_urls = ["https://en.wikipedia.org/wiki/Lists_of_populated_places_in_the_United_States"]
    base_href = "https://en.wikipedia.org"
    
    def start_requests(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            filename='yellowpage.log',
                            filemode='w+')        
        #proxy_meta = "83.149.70.159:13082"
        #yield Request(self.start_urls[0],self.parse_states,headers={'User-Agent': random.choice(agents)},meta={"proxy":proxy_meta})
        yield Request(self.start_urls[0],self.parse_states,headers={'User-Agent': random.choice(agents)})
    
    def parse_states(self,response):
        cities = [j for j in response.xpath("//a[contains(@title,'List of cities') or contains(@title,'List of muni')]")]
        for c in cities:
            the_href = c.xpath("./@href").extract_first()
            if the_href.find("_in_") > 0:
                text = c.xpath("./text()").extract_first()
                the_link =  self.base_href + the_href
                if the_link.find("by_population") < 0:
                    yield Request(the_link, self.parse_city,headers={'User-Agent': random.choice(agents)})
                

    def parse_city(self,response):
        res2 = response.xpath("//table[contains(@class,'wikitable')]/tr")
        for r in res2:
            city = r.xpath("./td/a/text()").extract_first()
            if city:
                yield {"type":"city","cityname":city,"source":response.request.url}
            

