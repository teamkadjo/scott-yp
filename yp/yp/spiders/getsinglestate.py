# -*- coding: utf-8 -*-
import random 
import os
import sys
import datetime
import time
import random 
import scrapy
from scrapy.http import Request
import urlparse
import logging
import django

from scrapy.exceptions import DropItem

print "Output is " + os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "..")

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "../../ypfront"))
os.environ['DJANGO_SETTINGS_MODULE'] = 'ypfront.settings'
django.setup()

from ypfront.models import CityInfo,StateInfo

agents = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.4; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.517 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
]


class SingleState(scrapy.Spider):
    name = 'singlestate'
    term = 'Columbus OH'    
    start_urls = ["https://en.wikipedia.org/wiki/List_of_cities_in_Ohio"]
    #start_urls = ['https://www.yellowpages.com/search?search_terms=roofing&geo_location_terms={}/'.format(term)]
    
    def start_requests(self):
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            filename='yellowpage.log',
                            filemode='w+')        
        proxy_meta = "108.59.14.208:13081"

        #cts = CityInfo.objects.all().order_by('id')
##########################################################a###################
        cts = CityInfo.objects.filter(stateinfo__statecode='WY').all()
#############################################################################
        count = 0
        for c in cts:           
            print c.cityname,c.stateinfo.statecode
            city = c.cityname.replace("_"," ")
            city = city.replace(u"\u2020", "").encode('utf-8').strip()
            #city = city.encode('ascii', 'replace').strip()
            state = c.stateinfo.statecode
            the_url = 'https://www.yellowpages.com/search?search_terms=painting&geo_location_terms={0}%2C+{1}/'.format(city,state)
            print the_url
            the_meta = {}
            the_meta['city'] = c.cityname
            #the_meta['proxy'] = proxy_meta
            the_meta['state'] = c.stateinfo.statecode
            yield Request(the_url,self.parse,headers={'User-Agent': random.choice(agents)},meta=the_meta)
            if count < 10:
                count = count + 1
            else:
                time.sleep(random.choice(range(40)) + 20)
                count = 0
        #yield Request(self.start_urls[0],self.parse_cities,headers={'User-Agent': random.choice(agents)},meta={"proxy":proxy_meta})
    
    def parse(self, response):
        logging.info("Parsing now this page : {}".format(response.request.url))
        address = urlparse.urlparse(response.url).netloc
        protocol = urlparse.urlparse(response.url).scheme
        for v in response.xpath("//div[@class='info']"):            
            if v.xpath("./h2/a/@href"):
                the_path = v.xpath("./h2/a/@href").extract_first()
                if not the_path.startswith("http"):
                    if the_path.startswith("/"):
                        print "{0}://{1}{2}".format(protocol,address,the_path)
                        url = "{0}://{1}{2}".format(protocol,address,the_path)
                    else:
                        print "{0}://{1}/{2}".format(protocol,address,the_path)
                        url = "{0}://{1}{2}".format(protocol,address,the_path)
                    yield Request(url,self.parse_detail,meta=response.request.meta)     
                    #time.sleep(random.choice(range(30)) + 10)
        next_page_result = response.xpath("//a[contains(@class,'next')]/@href").extract_first()
        if next_page_result:
            next_page = "{0}://{1}{2}".format(protocol,address,response.xpath("//a[contains(@class,'next')]/@href").extract_first())
            logging.info( "The next page is {}".format(next_page))
            yield Request(next_page,meta=response.request.meta)            


    def parse_detail(self,response):        
        business_name = response.xpath("//article[contains(@class,'business-card')]/div/h1/text()").extract_first()
        phone = response.xpath("//article[contains(@class,'business-card')]/section[@class='primary-info']/div/p[@class='phone']/text()").extract_first()
        res = response.xpath("//article[contains(@class,'business-card')]/section[@class='primary-info']/div/div[@class='time-info']")
        status_text = ','.join(res.css("*::text").extract())
        website_link = response.xpath("//div[contains(@class,'business-card-footer')]/a[contains(@class,'website-link')]/@href").extract_first()
        address =  ''.join(response.xpath("//p[@class='address']/descendant::*/text()").extract())
        if website_link is None:
            website_link = ""        
        email = response.xpath("//div[contains(@class,'business-card-footer')]/a[contains(@class,'email-business')]/@href").extract_first()
        if email is None:
            email = ""
        yield {"business_name":business_name,"phone":phone,"status_text":status_text,"url":response.url,"website_link":website_link,"email":email,"place":response.request.meta['city'],"state": response.request.meta['state'],"yp_orig_link":response.request.url,"address":address,"type":"yp"}
