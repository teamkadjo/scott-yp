"""
    ypfront URL Configuration
"""
from django.conf.urls import url
from django.contrib import admin

admin.site.site_header = "ypScraper Admin"


urlpatterns = [
    url(r'^admin/', admin.site.urls),
]
