from rest_framework import serializers
from ypfront.models import *

class CityInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CityInfo 
        fields = '__all__'
        
