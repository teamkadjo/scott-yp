# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-07-10 02:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ypfront', '0015_stateinfo_is_scraped'),
        ('ypfront', '0015_merge_20180710_0225'),
    ]

    operations = [
    ]
