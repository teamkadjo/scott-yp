# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.admin import SimpleListFilter

import sys
reload(sys)
sys.setdefaultencoding('utf8')


class BaseModel(models.Model):

    created_at = models.DateTimeField(auto_now = True)
    updated_at = models.DateTimeField(default = timezone.now)

    class Meta:
        abstract = True


    
class StateInfo(BaseModel):
    statecode = models.CharField(max_length=10, default = '',null=True)
    statename = models.CharField(max_length=255, default = '',null=True)
    is_scraped = models.NullBooleanField(default=False,null=True)
    def __str__(self):
        return self.statename + " (" + self.statecode  + ")"


class StateInfoAdmin(admin.ModelAdmin):
    list_display = ('statecode','statename')
    search_fields = ('statecode','statename')
    
class CityInfo(BaseModel):
    cityname =  models.CharField(max_length=255, default = '',null=True)
    source =  models.CharField(max_length=255, default = '',null=True)
    state = models.CharField(max_length=255, default = '',null=True)
    state_abrv = models.CharField(max_length=2, default = '',null=True)
    stateinfo = models.ForeignKey(StateInfo,default=None,null=True)
    class Meta:
        unique_together = ('cityname', 'source',)   


class CityInfoAdmin(admin.ModelAdmin):
    list_display = ('cityname','state','source','stateinfo')
    search_fields = ('cityname','source','state')    



class EntityInformation(BaseModel):
    business_name = models.CharField(max_length=255, default = '',null=True)
    phone = models.CharField(max_length=100, default = '',null=True)
    status = models.CharField(max_length=255, default = '',null=True)
    website = models.TextField(default='',null=True)
    email = models.CharField(max_length=255, default = '',null=True)
    place = models.CharField(max_length=255, default = '',null=True)
    address = models.CharField(max_length=255, default = '',null=True)
    yp_link = models.TextField(default='',null=True)
    source = models.CharField(max_length=255, default = '',null=True)
    state = models.ForeignKey(StateInfo,null=True,default=None)
    scrape_type = models.CharField(max_length=10, default = '',null=True)
    search_term = models.CharField(max_length=255, default = 'Roofing',null=True)
    is_with_portal = models.NullBooleanField(default=False,null=True)
    customer_portal = models.TextField(default='',null=True)
    customer_portal_level2 = models.TextField(default='',null=True)
    class Meta:
        unique_together = ('phone', 'email',)    



class EmailFilter(SimpleListFilter):
    title = 'email' # or use _('country') for translated title
    parameter_name = 'email'

    def lookups(self, request, model_admin):
        
        return [('1','With Email'),('2','No Email')]


    def queryset(self, request, queryset):
        if self.value():
            print "filtervalue is " + self.value()
            if self.value() =='1':
                return queryset.exclude(email='')
            else:
                return queryset.filter(email='')
        else:
            return queryset

class PortalFilter(SimpleListFilter):
    title = 'portal' # or use _('country') for translated title
    parameter_name = 'portal'

    def lookups(self, request, model_admin):
        
        return [('1','With Portal'),('2','Without Portal')]


    def queryset(self, request, queryset):
        if self.value():
            if self.value() =='2':
                return queryset.exclude(is_with_portal=True)
            else:
                return queryset.filter(is_with_portal=True).exclude(customer_portal='')
        else:
            return queryset

class CategoryFilter(SimpleListFilter):
    title = 'category' # or use _('country') for translated title
    parameter_name = 'category'

    def lookups(self, request, model_admin):        
        return [('1','Roofing'),('2','Painting'),('3','Pest Control')]


    def queryset(self, request, queryset):
        if self.value():
            print "filtervalue is " + self.value()
            if self.value() =='1':
                return queryset.filter(search_term='Roofing')
            elif self.value() == '2':
                return queryset.filter(search_term='painting')
            elif self.value() == '3':
                return queryset.filter(search_term='pest control')
 
            elif self.value() == "0":
                return None
        else:
            return queryset

class EntityInformationAdmin(admin.ModelAdmin):
    actions = ['download_csv']
    list_display = ('business_name','phone','email','address','place','state','search_term')
    search_fields = ('business_name','phone','email','website','place','state__statename') 
    list_filter = (EmailFilter,CategoryFilter,PortalFilter)
    
    def download_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse
        f = open('yp-roofing.csv', 'wb')
        writer = csv.writer(f)
        xstr = lambda s: '' if s is None else str(s).decode('utf-8')
        writer.writerow(["id", "business_name", "phone", "website", "email","place","address","yp_link","source","state","search_term"])

        for s in queryset:
            the_row = [s.id,s.business_name,s.phone,s.website,s.email,s.place,s.address,s.yp_link,s.source,s.state,s.search_term]
            fixed_row = [xstr(s) for s in the_row]
            writer.writerow(fixed_row)
        f.close()

        f = open('yp-roofing.csv', 'r')
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=yp-roofing-downloads.csv'
        return response
    download_csv.short_description = "Download CSV file for selected stats."
    
