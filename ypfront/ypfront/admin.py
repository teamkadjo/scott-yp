from django.contrib import admin
from .models import *
admin.site.register(EntityInformation,EntityInformationAdmin)
admin.site.register(StateInfo,StateInfoAdmin)
admin.site.register(CityInfo,CityInfoAdmin)
